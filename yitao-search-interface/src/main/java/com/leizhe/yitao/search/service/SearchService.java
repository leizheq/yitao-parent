package com.leizhe.yitao.search.service;

import com.leizhe.yitao.domain.Category;
import com.leizhe.yitao.domain.Spu;
import com.leizhe.yitao.search.entity.Goods;
import com.leizhe.yitao.search.entity.SearchRequest;
import com.leizhe.yitao.search.entity.SearchResult;
import org.elasticsearch.search.aggregations.bucket.terms.LongTerms;

import java.util.List;

/**
 * @ProjectName: yitao-parent
 * @Auther: leizhe
 * @Date: 2019/4/23 18:21
 * @Description:
 */
public interface SearchService {
    Goods buildGoods(Spu spu);
    SearchResult<Goods> search(SearchRequest searchRequest);
    List<Category> handleCategoryAgg(LongTerms terms);
    void insertOrUpdate(Long id);
    void delete(Long id);


    // 构建货物 （测试使用）
    Goods buildGoods2(Spu spu);
}
