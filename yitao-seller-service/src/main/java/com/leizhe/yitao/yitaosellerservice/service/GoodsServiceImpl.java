package com.leizhe.yitao.yitaosellerservice.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.leizhe.yitao.common.exception.ServiceException;
import com.leizhe.yitao.domain.*;
import com.leizhe.yitao.dto.CartDto;
import com.leizhe.yitao.entity.PageResult;
import com.leizhe.yitao.mapper.*;
import com.leizhe.yitao.upload.service.GoodsService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.beans.Transient;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 商品业务类
 */
@Slf4j
@Service(timeout = 3000)
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private SpuMapper spuMapper;

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private BrandMapper brandMapper;

    @Autowired
    private SpuDetailMapper spuDetailMapper;

    @Autowired
    private SkuMapper skuMapper;

    @Autowired
    private StockMapper stockMapper;

    @Autowired
    private AmqpTemplate amqpTemplate;


    /**
     * 商品查询
     * @param page
     * @param rows
     * @param key
     * @param saleable
     * @return
     */
    @Override
    public PageResult<Spu> querySpuByPage(Integer page, Integer rows, String key, Boolean saleable) {
        Example example = new Example(Spu.class);
        Example.Criteria criteria = example.createCriteria();
        // 条件查询
        if (StringUtils.isNotBlank(key)){
            criteria.andLike("title","%"+key+"%");
        }
        // 是否上架
        if (saleable != null && !"".equals(saleable)){
            criteria.andEqualTo("saleable",saleable ? 1 : 0);
        }
        // 判断是否查询所有
        if (rows < 0){
            rows = spuMapper.selectCountByExample(example);
        }
        // 排序
        example.setOrderByClause(" last_update_time DESC");
        // 只查询有效的商品（无效的代表已经被删除）
        criteria.andEqualTo("valid" , 1);
        //开始查询
        PageHelper.startPage(page, rows);
        List<Spu> spuList = spuMapper.selectByExample(example);
        if (CollectionUtils.isEmpty(spuList)){
            throw new ServiceException("查询的商品不存在");
        }
        // 对查询结果中的分类名和品牌名进行处理
        handleCategoryAndBrand(spuList);

        // 返回结果
        PageInfo<Spu> spuPageInfo = new PageInfo<>(spuList);
        return new PageResult<>(spuPageInfo.getTotal(),spuList);
    }

    private void handleCategoryAndBrand(List<Spu> spuList) {

        for (Spu spu : spuList) {
            // 类别
            List<Category> categories = categoryMapper.selectByIdList(Arrays.asList(spu.getCid1(), spu.getCid2(), spu.getCid3()));
            List<String> collect = categories.stream().map(Category::getName).collect(Collectors.toList());
            spu.setBname(StringUtils.join(collect,","));

            // 品牌
            spu.setCname(brandMapper.selectByPrimaryKey(spu.getBrandId()).getName());
        }
    }


    @Override
    public SpuDetail querySpuDetailBySpuId(Long spuId) {
        SpuDetail spuDetail = spuDetailMapper.selectByPrimaryKey(spuId);
        if (spuDetail == null) {
            throw new ServiceException("查询的商品不存在");
        }
        return spuDetail;
    }

    @Override
    public List<Sku> querySkuBySpuId(Long spuId) {
        Sku sku = new Sku();
        sku.setSpuId(spuId);
        List<Sku> skuList = skuMapper.select(sku);
        if (CollectionUtils.isEmpty(skuList)) {
            throw new ServiceException("查询的商品的SKU失败");
        }

        //查询库存
        for (Sku sku1 : skuList) {
            sku1.setStock(stockMapper.selectByPrimaryKey(sku1.getId()).getStock());
        }
        return skuList;
    }

    @Transactional
    @Override
    public void deleteGoodsBySpuId(Long spuId) {
        if (spuId == null) {
            throw new ServiceException("请传入查询的SPU编号");
        }
        //删除spu,把spu中的valid字段设置成false
        Spu spu = new Spu();
        spu.setId(spuId);
        spu.setValid(false);
        spu.setSaleable(false);
        int count = spuMapper.updateByPrimaryKeySelective(spu);
        if (count == 0) {
            throw new ServiceException("删除SPU失败");
        }

        //发送消息
        sendMessage(spuId, "delete");
    }

    /**
     * 增加商品
     * @param spu
     */
    @Transient
    @Override
    public void addGoods(Spu spu) {
        //添加商品要添加四个表 spu, spuDetail, sku, stock四张表
        spu.setSaleable(true);
        spu.setValid(true);
        spu.setCreateTime(new Date());
        spu.setLastUpdateTime(spu.getCreateTime());
        // 插入数据
        int count = spuMapper.insert(spu);
        if (count != 1){
            throw new ServiceException("添加商品信息失败");
        }
        // 插入 spuDetail 数据
        SpuDetail spuDetail = spu.getSpuDetail();
        spuDetail.setSpuId(spu.getId());
        spuDetailMapper.insert(spuDetail);

        //插入sku和库存
        saveSkuAndStock(spu);

        sendMessage(spu.getId(),"insert");

    }


    /**
     * 商品信息更新
     * @param spu
     */
    @Transactional
    @Override
    public void updateGoods(Spu spu) {
        if (spu.getId() == 0) {
            throw new ServiceException("请传入查询的商品ID");
        }
        //首先查询sku
        Sku sku = new Sku();
        sku.setSpuId(spu.getId());
        List<Sku> skuList = skuMapper.select(sku);
        if (!CollectionUtils.isEmpty(skuList)) {
            //删除所有sku
            skuMapper.deleteBYSpuid(spu.getId());
            //删除库存
            List<Long> ids = skuList.stream()
                    .map(Sku::getId)
                    .collect(Collectors.toList());
            stockMapper.deleteByIdList(ids);
        }
        //更新数据库  spu  spuDetail
        spu.setLastUpdateTime(new Date());
        //更新spu spuDetail
        int count = spuMapper.updateByPrimaryKeySelective(spu);
        if (count == 0) {
            throw new ServiceException("更新商品信息失败");
        }


        SpuDetail spuDetail = spu.getSpuDetail();
        spuDetail.setSpuId(spu.getId());
        count = spuDetailMapper.updateByPrimaryKeySelective(spuDetail);
        if (count == 0) {
            throw new ServiceException("更新商品详情信息失败");
        }

        //更新sku和stock
        saveSkuAndStock(spu);

        //发送消息
        sendMessage(spu.getId(), "update");
    }


    /**
     * 商品上架
     * @param spu
     */
    @Override
    public void handleSaleable(Spu spu) {
        spu.setSaleable(!spu.getSaleable());
        int count = spuMapper.updateByPrimaryKeySelective(spu);
        if (count != 1) {
            throw new ServiceException("商品上架失败");
        }

        //发送消息
        sendMessage(spu.getId(), "update");
    }

    @Override
    public Spu querySpuBySpuId(Long spuId) {
        //根据spuId查询spu
        Spu spu = spuMapper.selectByPrimaryKey(spuId);

        //查询spuDetail
        SpuDetail detail = querySpuDetailBySpuId(spuId);

        //查询skus
        List<Sku> skus = querySkuBySpuId(spuId);

        spu.setSpuDetail(detail);
        spu.setSkus(skus);

        return spu;

    }

    @Override
    public List<Sku> querySkusByIds(List<Long> ids) {
        List<Sku> skus = skuMapper.selectByIdList(ids);
        if (CollectionUtils.isEmpty(skus)) {
            throw new ServiceException("查询");
        }
        //填充库存
        fillStock(ids, skus);
        return skus;
    }

    @Transactional
    @Override
    public void decreaseStock(List<CartDto> cartDtos) {
        for (CartDto cartDto : cartDtos) {
            int count = stockMapper.decreaseStock(cartDto.getSkuId(), cartDto.getNum());
            if (count != 1) {
                throw new ServiceException("扣减库存失败");
            }
        }
    }

    private void fillStock(List<Long> ids, List<Sku> skus) {
        //批量查询库存
        List<Stock> stocks = stockMapper.selectByIdList(ids);
        if (CollectionUtils.isEmpty(stocks)) {
            throw new ServiceException("保存库存失败");
        }
        //首先将库存转换为map，key为sku的ID
        Map<Long, Integer> map = stocks.stream().collect(Collectors.toMap(s -> s.getSkuId(), s -> s.getStock()));

        //遍历skus，并填充库存
        for (Sku sku : skus) {
            sku.setStock(map.get(sku.getId()));
        }
    }


    /**
     * 保存sku和库存
     *
     * @param spu
     */
    private void saveSkuAndStock(Spu spu) {
        List<Sku> skus = spu.getSkus();

        ArrayList<Stock> stocks = new ArrayList<>();

        // 循环插入sku，并获取库存数据 用于存储
        for (Sku sku : skus) {
            sku.setSpuId(spu.getId());
            sku.setCreateTime(new Date());
            sku.setLastUpdateTime(sku.getCreateTime());
            int count = skuMapper.insert(sku);
            if (count != 1){
                throw new ServiceException("保存spu对应的sku失败");
            }
            Stock stock = new Stock();
            stock.setSkuId(sku.getId());
            stock.setStock(sku.getStock());
            stocks.add(stock);
        }
        // 插入库存
        int count = stockMapper.insertList(stocks);
        if (count == 0){
            throw new ServiceException("保存商品的SKU库存失败");
        }
    }




    /**
     * 封装发送到消息队列的方法
     *
     * @param id
     * @param type
     */
    private void sendMessage(Long id, String type) {
        try {
            amqpTemplate.convertAndSend("item." + type, id);
            System.out.println("发送商品"+type+"操作信息.......");
        } catch (Exception e) {
            log.error("{}商品消息发送异常，商品ID：{}", type, id, e);
        }
    }
}
