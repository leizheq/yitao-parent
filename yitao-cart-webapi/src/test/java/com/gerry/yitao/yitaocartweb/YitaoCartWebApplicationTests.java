package com.leizhe.yitao.yitaocartweb;

import com.leizhe.yitao.yitaocartservicewebapi.YitaoCartWebApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {YitaoCartWebApplication.class})
public class YitaoCartWebApplicationTests {



    @Test
    public void contextLoads() {
    }

}
