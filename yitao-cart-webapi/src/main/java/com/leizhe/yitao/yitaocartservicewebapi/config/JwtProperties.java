package com.leizhe.yitao.yitaocartservicewebapi.config;

import com.leizhe.yitao.common.util.RsaUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;
import java.security.PublicKey;

/**
 * @ProjectName: yitao-parent
 * @Auther: leizhe
 * @Date: 2019/5/9 19:17
 * @Description:
 */
@Data
@Slf4j
@ConfigurationProperties(prefix = "yt.jwt")
public class JwtProperties {

    private String pubKeyPath;

    private String cookieName;

    private PublicKey publicKey;

    @PostConstruct
    public void init() {
        try {
            publicKey = RsaUtils.getPublicKey(pubKeyPath);
        } catch (Exception e) {
            log.error("初始化公钥失败", e);
            throw new RuntimeException();

        }
    }
}
