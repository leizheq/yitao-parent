package com.leizhe.yitao.yitaoauthservice;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import com.leizhe.yitao.auth.entity.JwtProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/*@SpringBootApplication(
        exclude = DataSourceAutoConfiguration.class)
@EnableDubbo
@EnableConfigurationProperties(JwtProperties.class)*/
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableDubbo
@EnableConfigurationProperties(JwtProperties.class)
public class YitaoAuthServiceApplication {


    public static void main(String[] args) {
        SpringApplication.run(YitaoAuthServiceApplication.class);
    }

}
