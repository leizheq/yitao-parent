package com.leizhe.yitao.common.base.interceptor.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @ProjectName: yitao-parent
 * @Auther: leizhe
 * @Date: 2019/5/8 11:55
 * @Description:
 */

@Data
@ConfigurationProperties(prefix = "yt.filter")
public class FilterProperties {
    private List<String> allowPaths;

}