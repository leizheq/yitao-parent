package com.leizhe.yitao.detail.service;

import java.util.Map;

/**
 * @ProjectName: yitao-parent
 * @Auther: leizhe
 * @Date: 2019/4/29 18:11
 * @Description:
 */
public interface PageDetailService {
    Map<String, Object> loadModel(Long spuId);
    Map<String, Object> loadModel2(Long spuId);
    void createHtml(Long spuId);
    void deleteHtml(Long id);

    // 异步
    void asyncExecute(Long spuId);
}
