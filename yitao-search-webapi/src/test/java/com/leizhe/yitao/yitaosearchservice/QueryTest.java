package com.leizhe.yitao.yitaosearchservice;

import com.alibaba.dubbo.config.annotation.Reference;
import com.google.common.base.Supplier;
import com.leizhe.yitao.domain.Brand;
import com.leizhe.yitao.domain.Category;
import com.leizhe.yitao.search.entity.Goods;
import com.leizhe.yitao.search.entity.SearchRequest;
import com.leizhe.yitao.search.entity.SearchResult;
import com.leizhe.yitao.search.service.SearchService;
import org.junit.Test;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QueryTest extends YitaoSellerServiceApplicationTests {
    @Reference(check = false,timeout = 3000)
    private SearchService searchService;


    @Test
    public void queryGoodsTest(@RequestBody SearchRequest searchRequest){
        HashMap<String, String> map = new HashMap<>();
        map.put("all","");
        SearchResult<Goods> search = searchService.search(searchRequest);
        List<Brand> brands = search.getBrands();
        List<Category> categories = search.getCategories();
        List<Map<String, Object>> specs = search.getSpecs();
        brands.stream().forEach(e -> System.out.println(e.toString()));

    }

}
