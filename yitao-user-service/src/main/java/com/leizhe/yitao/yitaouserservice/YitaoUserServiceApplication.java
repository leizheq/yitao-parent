package com.leizhe.yitao.yitaouserservice;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import com.codingapi.txlcn.tc.config.EnableDistributedTransaction;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@EnableDubbo
@MapperScan(basePackages = "com.leizhe.yitao.mapper")
@EnableDistributedTransaction
public class YitaoUserServiceApplication {


    public static void main(String[] args) {
        SpringApplication.run(YitaoUserServiceApplication.class);
    }

}
