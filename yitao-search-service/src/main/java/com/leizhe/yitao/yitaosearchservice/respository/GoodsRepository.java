package com.leizhe.yitao.yitaosearchservice.respository;

import com.leizhe.yitao.search.entity.Goods;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @ProjectName: 将数据保存到Elasticsearch 继承的是ElasticsearchRepository
 * 存储的数据声明在 Goods 的实体注解里面
 * @Auther: leizhe
 * @Date: 2019/4/23 18:23
 * @Description:
 */
public interface GoodsRepository extends ElasticsearchRepository<Goods, Long> {
}
