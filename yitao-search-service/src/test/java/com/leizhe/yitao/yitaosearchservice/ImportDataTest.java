package com.leizhe.yitao.yitaosearchservice;

import com.leizhe.yitao.domain.Spu;
import com.leizhe.yitao.entity.PageResult;
import com.leizhe.yitao.search.entity.Goods;
import com.leizhe.yitao.search.service.SearchService;
import com.leizhe.yitao.yitaosearchservice.client.GoodsClient;
import com.leizhe.yitao.yitaosearchservice.respository.GoodsRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * @ProjectName: yitao-parent
 * @Auther: leizhe
 * @Date: 2019/4/24 16:17
 * @Description:
 */
public class ImportDataTest extends YitaoSearchApplicationTests {
    @Autowired
    private GoodsClient goodsClient;

    @Autowired(required = false)
    private SearchService searchService;

    @Autowired
    private GoodsRepository goodsRepository;

    /**
     * 初次加载数据到es
     */
    @Test
    public void loadData(){
        int page = 1;
        int size = 0;
        int rows = 100;

        do {
            PageResult<Spu> result = goodsClient.querySpuByPage(page, rows, null, true);
            List<Spu> spus = result.getRows();
            size = spus.size();
            ArrayList<Goods> goodsList = new ArrayList<>();
            for (Spu spu : spus) {
                try {
                    Goods goods = searchService.buildGoods(spu);
                    goodsList.add(goods);
                } catch (Exception e) {
                    e.printStackTrace();
                    break;
                }
            }
            this.goodsRepository.saveAll(goodsList);
            page++;
        } while (size == 100);
    }

    @Test
    public void testQuery(){
        PageResult<Spu> spuPageResult = goodsClient.querySpuByPage2(1, 10, null, true);
        List<Spu> spus = spuPageResult.getRows();


        Goods goods = searchService.buildGoods2(spus.get(0));


        goodsRepository.save(goods);
    }
}
