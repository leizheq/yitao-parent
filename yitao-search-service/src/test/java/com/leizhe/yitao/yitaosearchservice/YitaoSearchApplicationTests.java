package com.leizhe.yitao.yitaosearchservice;

import com.leizhe.yitao.yitaosearchservice.YitaoSearchServiceApplication;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = YitaoSearchServiceApplication.class)
public class YitaoSearchApplicationTests {
}
