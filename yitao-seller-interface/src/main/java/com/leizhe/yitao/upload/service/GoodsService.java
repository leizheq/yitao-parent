package com.leizhe.yitao.upload.service;

import com.leizhe.yitao.domain.Sku;
import com.leizhe.yitao.domain.Spu;
import com.leizhe.yitao.domain.SpuDetail;
import com.leizhe.yitao.dto.CartDto;
import com.leizhe.yitao.entity.PageResult;

import java.util.List;

public interface GoodsService {
    PageResult<Spu> querySpuByPage(Integer page, Integer rows, String key, Boolean saleable);

    SpuDetail querySpuDetailBySpuId(Long spuId);

    List<Sku> querySkuBySpuId(Long spuId);

    void deleteGoodsBySpuId(Long spuId);

    void addGoods(Spu spu);

    void updateGoods(Spu spu);

    void handleSaleable(Spu spu);

    Spu querySpuBySpuId(Long spuId);

    List<Sku> querySkusByIds(List<Long> ids);

    void decreaseStock(List<CartDto> cartDtos);
}
