package com.leizhe.yitao.upload.service;

import com.leizhe.yitao.bo.BrandBo;
import com.leizhe.yitao.domain.Brand;
import com.leizhe.yitao.domain.Category;
import com.leizhe.yitao.entity.PageResult;

import java.util.List;


public interface BrandService {

    PageResult<Brand> queryBrandByPageAndSort(Integer page, Integer rows, String sortBy, Boolean desc, String key);

    void saveBrand(Brand brand, List<Long> cids);

    List<Category> queryCategoryByBid(Long bid);

    void updateBrand(BrandBo brandbo);

    void deleteBrand(Long bid);

    List<Brand> queryBrandByCid(Long cid);

    Brand queryBrandByBid(Long id);

    List<Brand> queryBrandByIds(List<Long> ids);

    // 用于测试分布式事务
    void addBrand(Brand brand);

    void add();


}
