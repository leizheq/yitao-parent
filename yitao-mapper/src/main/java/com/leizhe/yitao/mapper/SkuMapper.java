package com.leizhe.yitao.mapper;

import com.leizhe.yitao.domain.Sku;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.additional.idlist.IdListMapper;
import tk.mybatis.mapper.common.Mapper;

/**
 * @ProjectName: yitao-parent
 * @Auther: leizhe
 * @Date: 2019/4/14 15:24
 * @Description:
 */

public interface SkuMapper extends Mapper<Sku>, IdListMapper<Sku, Long> {

    @Delete("delete from tb_sku where spu_id = #{spuId}")
    void deleteBYSpuid(@Param("spuId") Long id);
}
