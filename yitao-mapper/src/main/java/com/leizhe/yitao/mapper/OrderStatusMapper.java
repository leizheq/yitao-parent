package com.leizhe.yitao.mapper;

import com.leizhe.yitao.common.base.mapper.BaseMapper;
import com.leizhe.yitao.domain.OrderStatus;

/**
 * @ProjectName: yitao-parent
 * @Auther: leizhe
 * @Date: 2019/4/14 15:22
 * @Description:
 */

public interface OrderStatusMapper extends BaseMapper<OrderStatus, Long> {
}
