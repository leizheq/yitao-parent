package com.leizhe.yitao.mapper;

import com.leizhe.yitao.common.base.mapper.BaseMapper;
import com.leizhe.yitao.domain.Spu;

/**
 * @ProjectName: yitao-parent
 * @Auther: leizhe
 * @Date: 2019/4/14 15:25
 * @Description:
 */

public interface SpuMapper extends BaseMapper<Spu, Long> {
}
