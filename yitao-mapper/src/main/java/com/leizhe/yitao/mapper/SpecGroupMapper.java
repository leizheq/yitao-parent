package com.leizhe.yitao.mapper;

import com.leizhe.yitao.domain.SpecGroup;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author bystander
 * @date 2018/9/18
 */
public interface SpecGroupMapper extends Mapper<SpecGroup> {
}
