package com.leizhe.yitao.mapper;

import com.leizhe.yitao.domain.Order;
import tk.mybatis.mapper.common.Mapper;

/**
 * @ProjectName: yitao-parent
 * @Auther: leizhe
 * @Date: 2019/4/14 15:21
 * @Description:
 */

public interface OrderMapper extends Mapper<Order> {
}
