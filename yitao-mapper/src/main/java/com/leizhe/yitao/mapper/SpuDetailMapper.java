package com.leizhe.yitao.mapper;

import com.leizhe.yitao.domain.SpuDetail;
import tk.mybatis.mapper.common.Mapper;

/**
 * @ProjectName: yitao-parent
 * @Auther: leizhe
 * @Date: 2019/4/14 15:26
 * @Description:
 */
public interface SpuDetailMapper extends Mapper<SpuDetail> {
}
