package com.leizhe.yitao.mapper;

import com.leizhe.yitao.domain.SpecParam;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author bystander
 * @date 2018/9/18
 */
public interface SpecParamMapper extends Mapper<SpecParam> {
}
