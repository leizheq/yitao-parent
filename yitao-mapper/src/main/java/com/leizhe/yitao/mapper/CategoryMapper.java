package com.leizhe.yitao.mapper;

import com.leizhe.yitao.domain.Category;
import tk.mybatis.mapper.additional.idlist.IdListMapper;
import tk.mybatis.mapper.common.Mapper;

/**
 * @ProjectName: yitao-parent
 * @Auther: leizhe
 * @Date: 2019/4/14 15:19
 * @Description:
 */

public interface CategoryMapper extends Mapper<Category>, IdListMapper<Category, Long> {
}
