package com.leizhe.yitao.mapper;

import com.leizhe.yitao.common.base.mapper.BaseMapper;
import com.leizhe.yitao.domain.User;

/**
 * @ProjectName: yitao-parent
 * @Auther: leizhe
 * @Date: 2019/4/14 15:28
 * @Description:
 */

public interface UserMapper extends BaseMapper<User, Long> {
}
