package com.leizhe.yitao.yitaodetailservice;

import com.leizhe.yitao.yitaosearchservice.YitaoDetailServiceApplication;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = YitaoDetailServiceApplication.class)
public class YitaoPageDetailApplicationTests {
}
